/**
 * Created by roque on 04/05/17.
 */
module.exports = {
    /*
     * @Author: Roque
     *
     */
    path: function () {
        //IP local
        return "http://localhost";
    },
    /*
     * @Author: Roque
     * Generates random value, eg: if you put 10000 a number will be generated up to 1000;
     */
    getValorLimitadoParametro: function (paramvalor) {
        return Math.floor(Math.random() * paramvalor);
    },
    /*
     * @Author: Roque
     * Function for generate aleatory number cpf
     */
    getRandomCpf: function () {
        let sort = Math.floor(Math.random() * 90000000000 + 10000000000);
        return ''+sort;
    },
    /*
     * @Author: Roque
     * Function for generate aleatory number
     */
    getRandomNumber: function () {
        let sort = Math.floor(Math.random() * 9000 + 1000);
        return ''+sort;
    },
    /*
     * @Author: Roque
     * Function for generate aleatory barcode
     */
    getRandomBarcode: function () {
        let sort = Math.floor(Math.random() * 90000000 + 10000000);
        return ''+sort;
    },
    /*
     * @Author: Roque
     * Function for generate Phone
     */
    getNumberPhone: function () {
        let sort = Math.floor(Math.random() * 90 + 10);
        let num = Math.floor(Math.random() * 9000 + 1000);
        return '999'+sort+'-'+num;
    },
    /**
     *
     * @Author: Roque
     * @returns Generate text
     */
    getText: function(){
        let letras = 'PRODUCT ';
        let sort = Math.floor(Math.random() * 9000 + 1000);
        return ''+letras+sort;
    },
    /**
     *
     * @Author: Roque
     * @returns Generate email
     */
    getEmail: function(){
        let letras = 'EmailTest';
        let sort = Math.floor(Math.random() * 90 + 10);
        return ''+letras+sort;
    },
    /**
     * @Author: Roque
     * @returns Product registration
     */

    toRegisterProductInvalidApplication: function(op){

        let page_addProduct = require('./Pages/Product/page_newProduct');

        let row = {
            inputValueProductName: this.getText(),
            inputValuePartNumber: this.getRandomNumber(),
            inputValueBarcode: this.getRandomBarcode()
        };

        if(op === 'name') {
            page_addProduct.waitScreenAddProduct();
            page_addProduct.enterFieldProductName(row.inputValueProductName + '@@@@');
            page_addProduct.enterFieldPartNumber(row.inputValuePartNumber);
            page_addProduct.enterFieldBarcode(row.inputValueBarcode);
            page_addProduct.enterFieldProductDescription('Description invalid Test');
            page_addProduct.enterFieldProductUnitMeasures('UNIT 1');
            page_addProduct.enterFieldProductCategory('CATEGORY 0');
            page_addProduct.enterFieldSupplier('SUPPLIER 0');
            page_addProduct.enterFieldProductBrand('BRAND 0');
            page_addProduct.clickButtonRegister();
            console.log('Dados:' + '\n' + 'Produto:' + row.inputValueProductName + '@@@@'+ '\n' + 'Número:' + row.inputValuePartNumber + '\n' + 'Código de Barras:' + row.inputValueBarcode);
        } else if(op === 'partNumber') {
            page_addProduct.waitScreenAddProduct();
            page_addProduct.enterFieldProductName(row.inputValueProductName);
            page_addProduct.enterFieldPartNumber(row.inputValuePartNumber + '@@@@');
            page_addProduct.enterFieldBarcode(row.inputValueBarcode);
            page_addProduct.enterFieldProductDescription('Description Test');
            page_addProduct.enterFieldProductUnitMeasures('UNIT 1');
            page_addProduct.enterFieldProductCategory('CATEGORY 0');
            page_addProduct.enterFieldSupplier('SUPPLIER 0');
            page_addProduct.enterFieldProductBrand('BRAND 0');
            page_addProduct.clickButtonRegister();
            console.log('Dados:' + '\n' + 'Produto:' + row.inputValueProductName + '\n' + 'Número:' + row.inputValuePartNumber + '@@@@' + '\n' + 'Código de Barras:' + row.inputValueBarcode);
        } else if(op === 'barcode') {
            page_addProduct.waitScreenAddProduct();
            page_addProduct.enterFieldProductName(row.inputValueProductName);
            page_addProduct.enterFieldPartNumber(row.inputValuePartNumber);
            page_addProduct.enterFieldBarcode(row.inputValueBarcode + '@@@@');
            page_addProduct.enterFieldProductDescription('Description Test');
            page_addProduct.enterFieldProductUnitMeasures('UNIT 1');
            page_addProduct.enterFieldProductCategory('CATEGORY 0');
            page_addProduct.enterFieldSupplier('SUPPLIER 0');
            page_addProduct.enterFieldProductBrand('BRAND 0');
            page_addProduct.clickButtonRegister();
            console.log('Dados:' + '\n' + 'Produto:' + row.inputValueProductName + '\n' + 'Número:' + row.inputValuePartNumber + '\n' + 'Código de Barras:' + row.inputValueBarcode + '@@@@');
        } else {
            page_addProduct.waitScreenAddProduct();
            page_addProduct.enterFieldProductName(op);
            page_addProduct.enterFieldPartNumber(row.inputValuePartNumber);
            page_addProduct.enterFieldBarcode(row.inputValueBarcode);
            page_addProduct.enterFieldProductDescription('Description Test');
            page_addProduct.enterFieldProductUnitMeasures('UNIT 1');
            page_addProduct.enterFieldProductCategory('CATEGORY 0');
            page_addProduct.enterFieldSupplier('SUPPLIER 0');
            page_addProduct.enterFieldProductBrand('BRAND 0');
            page_addProduct.clickButtonRegister();
            console.log('Dados:' + '\n' + 'Produto:' + row.inputValueProductName + '\n' + 'Número:' + row.inputValuePartNumber + '\n' + 'Código de Barras:' + row.inputValueBarcode);
        }
    },

    toRegisterProductApplication: function(op){
        let page_addProduct = require('./Pages/Product/page_newProduct');
        let row = {
            inputValueProductName: this.getText(),
            inputValuePartNumber: this.getRandomNumber(),
            inputValueBarcode: this.getRandomBarcode()
        };
        if(op === 'hard') {
                page_addProduct.waitScreenAddProduct();
                page_addProduct.enterFieldProductName(row.inputValueProductName);
                page_addProduct.enterFieldPartNumber(row.inputValuePartNumber);
                page_addProduct.enterFieldBarcode(row.inputValueBarcode);
                page_addProduct.enterFieldProductDescription('Description Test');
                page_addProduct.enterFieldProductUnitMeasures('UNIT 0');
                page_addProduct.enterFieldProductCategory('CATEGORY 0');
                page_addProduct.enterFieldSupplier('SUPPLIER 0');
                page_addProduct.enterFieldProductBrand('BRAND 0');
                page_addProduct.clickButtonRegister();
                console.log('Dados:' + '\n' + 'Produto:' + row.inputValueProductName + '\n' + 'Número:' + row.inputValuePartNumber + '\n' + 'Código de Barras:' + row.inputValueBarcode);

        } else if(op === 'category') {
                page_addProduct.waitScreenAddProduct();
                page_addProduct.enterFieldProductName(row.inputValueProductName);
                page_addProduct.enterFieldPartNumber(row.inputValuePartNumber);
                page_addProduct.enterFieldBarcode(row.inputValueBarcode);
                page_addProduct.enterFieldProductDescription('Description Test');
                page_addProduct.enterFieldProductUnitMeasures('UNIT 0');
                page_addProduct.enterFieldProductCategory('CATEGORY 0');
                page_addProduct.clickButtonRegister();
                console.log('Dados:' + '\n' + 'Produto:' + row.inputValueProductName + '\n' + 'Número:' + row.inputValuePartNumber + '\n' + 'Código de Barras:' + row.inputValueBarcode);
        } else if(op === 'brand') {
                page_addProduct.waitScreenAddProduct();
                page_addProduct.enterFieldProductName(row.inputValueProductName);
                page_addProduct.enterFieldPartNumber(row.inputValuePartNumber);
                page_addProduct.enterFieldBarcode(row.inputValueBarcode);
                page_addProduct.enterFieldProductDescription('Description Test');
                page_addProduct.enterFieldProductUnitMeasures('UNIT 0');
                page_addProduct.enterFieldProductBrand('BRAND 0');
                page_addProduct.clickButtonRegister();
                console.log('Dados:' + '\n' + 'Produto:' + row.inputValueProductName + '\n' + 'Número:' + row.inputValuePartNumber + '\n' + 'Código de Barras:' + row.inputValueBarcode);
        } else if(op === 'supplier') {
                page_addProduct.waitScreenAddProduct();
                page_addProduct.enterFieldProductName(row.inputValueProductName);
                page_addProduct.enterFieldPartNumber(row.inputValuePartNumber);
                page_addProduct.enterFieldBarcode(row.inputValueBarcode);
                page_addProduct.enterFieldProductDescription('Description Test');
                page_addProduct.enterFieldProductUnitMeasures('UNIT 0');
                page_addProduct.enterFieldSupplier('SUPPLIER 0');
                page_addProduct.clickButtonRegister();
                console.log('Dados:' + '\n' + 'Produto:' + row.inputValueProductName + '\n' + 'Número:' + row.inputValuePartNumber + '\n' + 'Código de Barras:' + row.inputValueBarcode);
        } else {
                page_addProduct.waitScreenAddProduct();
                page_addProduct.enterFieldProductName(row.inputValueProductName);
                page_addProduct.enterFieldPartNumber(row.inputValuePartNumber);
                page_addProduct.enterFieldBarcode(row.inputValueBarcode);
                page_addProduct.enterFieldProductDescription('Description Test');
                page_addProduct.enterFieldProductUnitMeasures('UNIT 0');
                page_addProduct.clickButtonRegister();
                console.log('Dados:' + '\n' + 'Produto:' + row.inputValueProductName + '\n' + 'Número:' + row.inputValuePartNumber + '\n' + 'Código de Barras:' + row.inputValueBarcode);
        }
        return row;
    },

    /**
     *
     * @Author: Roque
     * @returns Generate text Category
     */
    getTextCategory: function(){
        let letras = 'CATEGORY ';
        let sort = Math.floor(Math.random() * 900 + 100);
        return ''+letras+sort;
    },


    /**
     * @Author: Roque
     * @returns Manage Category registration
     */

    toRegisterProductCategoryApplication: function(op){
        let page_manageCategories = require('./Pages/Product/page_manageCategories');
        let txtCategory = this.getTextCategory();

        let row = {
            inputValueCategoryName: txtCategory,
            inputValueCategoryDescription: `DESCRIPTION ${txtCategory}`
        };

        if(op === 'valid'){
            page_manageCategories.enterFieldCategoryName(row.inputValueCategoryName);
            page_manageCategories.enterFieldDescriptionName(row.inputValueCategoryDescription);
            page_manageCategories.clickButtonRegister();
        }
        console.log('Values: ' + '\n' + 'Category: ' + row.inputValueCategoryName + '\n' + 'Description: ' + row.inputValueCategoryDescription);
        return row;
    },

    /**
     *
     * @Author: Roque
     * @returns Generate text Brands
     */
    getTextBrand: function(){
        let letras = 'BRAND ';
        let sort = Math.floor(Math.random() * 900 + 100);
        return ''+letras+sort;
    },

    /**
     * @Author: Roque
     * @returns Manage Brands registration
     */

    toRegisterProductBrandApplication: function(op){
        let txtBrand = this.getTextBrand();
        let page_manageBrand = require('./Pages/Product/page_manageBrand');

        let row = {
            inputValueBrandName: txtBrand,
            inputValueBrandDescription: `DESCRIPTION ${txtBrand}`
        };

        if(op === 'valid'){
            page_manageBrand.enterFieldBrandName(row.inputValueBrandName);
            page_manageBrand.enterFieldBrandDescription(row.inputValueBrandDescription);
            page_manageBrand.clickButtonRegister();
        }
        console.log('Values: ' + '\n' + 'Brand: ' + row.inputValueBrandName + '\n' + 'Description: ' + row.inputValueBrandDescription);
        return row;
    },

    /**
     *
     * @Author: Roque
     * @returns Generate text Unit
     */
    getTextUnit: function(){
        let letras = 'UNIT ';
        let sort = Math.floor(Math.random() * 900 + 100);
        return ''+letras+sort;
    },

    /**
     * @Author: Roque
     * @returns Manage units registration
     */

    toRegisterProductUnitsApplication: function(op){
        let txtUnit = this.getTextUnit();
        let page_manageUnit = require('./Pages/Product/page_manageUnit');

        let row = {
            inputValueUnitName: txtUnit,
            inputValueUnitDescription: `DESCRIPTION ${txtUnit}`
        };

        if(op === 'valid'){
            page_manageUnit.enterFieldUnitName(row.inputValueUnitName);
            page_manageUnit.enterFieldUnitDescription(row.inputValueUnitName);
            page_manageUnit.clickButtonRegister();
        }
        console.log('Values: ' + '\n' + 'Unit: ' + row.inputValueUnitName + '\n' + 'Description: ' + row.inputValueUnitDescription);
        return row;
    },

    /**
     *
     * @Author: Roque
     * @returns Generate text Store
     */
    getTextStore: function(){
        let letras = 'STORE ';
        let sort = Math.floor(Math.random() * 900 + 100);
        return ''+letras+sort;
    },
    /**
     * @Author: Roque
     * @returns store registration
     */

    toRegisterStoreApplication: function(op){
        let txtStore = this.getTextStore();
        let page_newStore = require('./Pages/Store/page_newStore');

        let row = {
            inputValueStoreName: txtStore,
            inputValueStoreDescription: `DESCRIPTION ${txtStore}`
        };

        if(op === 'valid'){
            page_newStore.enterFieldStoreName(row.inputValueStoreName);
            page_newStore.enterFieldStoreDescription(row.inputValueStoreDescription);
            page_newStore.enterFieldStoreTaxUnits('R$');
            page_newStore.enterFieldStoreCountry('Brazil');
            page_newStore.enterFieldStoreState('PARAIBA');
            page_newStore.enterFieldStoreCity('JOAO PESSOA');
            page_newStore.clickButtonRegister();
        } else if(op === 'invalidName') {
            page_newStore.enterFieldStoreName('@@@@');
            page_newStore.enterFieldStoreDescription(row.inputValueStoreDescription);
            page_newStore.enterFieldStoreTaxUnits('R$');
            page_newStore.enterFieldStoreCountry('Brazil');
            page_newStore.enterFieldStoreState('PARAIBA');
            page_newStore.enterFieldStoreCity('JOAO PESSOA');
            page_newStore.clickButtonRegister();
            console.log('Values: ' + '\n' + 'Store: ' + '@@@' + '\n' + 'Description: ' + row.inputValueStoreDescription);
        }
        console.log('Values: ' + '\n' + 'Store: ' + row.inputValueStoreName + '\n' + 'Description: ' + row.inputValueStoreDescription);
        return row;
    },
    /**
     *
     * @Author: Roque
     * @returns Generate text Department
     */
    getTextDepartment: function(){
        let letras = 'DEPARTMENT ';
        let sort = Math.floor(Math.random() * 900 + 100);
        return ''+letras+sort;
    },

    /**
     * @Author: Roque
     * @returns department registration
     */

    toRegisterDepartmentApplication: function(op){
        let txtDepartment = this.getTextDepartment();
        let page_managerDepartment = require('./Pages/Store/page_managerDepartment');

        let row = {
            inputValueDepartmentName: txtDepartment,
            inputValueDepartmentDescription: `DESCRIPTION ${txtDepartment}`
        };
        if(op === 'valid') {
            page_managerDepartment.enterFieldDepartmentName(row.inputValueDepartmentName);
            page_managerDepartment.enterFieldDepartmentDescription(row.inputValueDepartmentDescription);
            page_managerDepartment.clickButtonRegister();
        }
        console.log('Values: ' + '\n' + 'Department: ' + row.inputValueDepartmentName + '\n' + 'Description: ' + row.inputValueDepartmentDescription);
        return row;
    },

    /**
     * @Author: Roque
     * @returns Hack registration
     */
    toRegisterHackApplication: function(numberHacks){
        let page_newHallHack = require('./Pages/HallHack/page_newHallHack');
        let count = 0;
        if(numberHacks !=0){
            do{
                count = count +1;
                page_newHallHack.enterFieldHack('Hack ' + count);
                page_newHallHack.clickButtonAddHack();
            }while(count < numberHacks){
                return count;
            }
        }else {
            console.log('Nenhum Rack foi adicionado');
        }
    },
    /**
     * @Author: Roque
     * @returns hall registration
     */

    toRegisterHallApplication: function(store, department, name, number){
        let page_newHallHack = require('./Pages/HallHack/page_newHallHack');
        let row = {
            inputValueHallName: name,
            inputValueHallCode: number,
        };
        page_newHallHack.enterFieldStore(store);
        page_newHallHack.enterFieldDepartment(department);
        page_newHallHack.enterFieldHallName(row.inputValueHallName);
        page_newHallHack.enterFieldHallCode(row.inputValueHallCode);
        page_newHallHack.enterFieldHallDescription(' Hall Description');
        page_newHallHack.clickButtonRegister();
        console.log('Dados:' + '\n' + 'HallName:' + row.inputValueHallName + '\n' + 'HallCode:' + row.inputValueHallCode);

        return row;
    },

    /**
     * @Author: Roque
     * @returns Login in To Application
     */
    loginAdminInToApplication: function(email,senha){
        let page_login = require('./Pages/Login/page_login');

        let row = {
            inputValueLoginEmail: email,
            inputValueLoginPassword: senha
        };

        page_login.waitScreenLogin();
        page_login.enterFieldEmail(row.inputValueLoginEmail);
        page_login.enterFieldPassword(row.inputValueLoginPassword);
        //page_login.validMenssage('');
        page_login.clickButtonLogin();
        // console.log('Dados:' + '\n' + 'Email:' + row.inputValueLoginEmail + '\n' + 'Password:' + row.inputValueLoginPassword);

        return row;
    },
    loginInToApplication: function(){
        let page_login = require('./Pages/Login/page_login');

        let row = {
            inputValueLoginEmail: 'admin@admin.com',
            inputValueLoginPassword: '1a2b3c'
        };

        page_login.waitScreenLogin();
        page_login.enterFieldEmail(row.inputValueLoginEmail);
        page_login.enterFieldPassword(row.inputValueLoginPassword);
        //page_login.validMenssage('');
        page_login.clickButtonLogin();
        expect(browser.getCurrentUrl()).toBe(this.path() + ':8080/');
        console.log('Dados:' + '\n' + 'Email:' + row.inputValueLoginEmail + '\n' + 'Password:' + row.inputValueLoginPassword);

        return row;
    },

    validMessage: function(msg){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            element.all(by.xpath('.//*[@id="message-span"]')).then(function(row){
                return row.map(function(element){
                    return element.getText().then(function(dateText){
                        console.log('Mensagem presente na tela: ',dateText);
                        if(dateText === msg){
                            console.log( { dateText: dateText, msg: msg});
                            return dateText;
                        }else{
                            expect(dateText).toBe(msg);
                            console.log('Erro: Mensagem esperada inválida');
                        }
                    });
                });
            }).then(function() {
                let el = element(by.id('message-span'));
                browser.wait(EC.visibilityOf(el), 5000)
            });
        }).catch('Erro');
    },

    validMessageField: function(msg){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            element.all(by.xpath(`//div[contains(.,'${msg}')]/span[1]`)).then(function(row){
                return row.map(function(element){
                    return element.getText().then(function(dateText){
                        console.log('dateText',dateText);
                        if(dateText === msg){
                            console.log( { dateText: dateText, msg: msg});
                            return dateText;
                        }else{
                            expect(dateText).toBe(msg);
                            console.log('Erro: Mensagem esperada inválida');
                        }
                    });
                });
            }).then(function() {
                let el = element(by.xpath(`//div[contains(.,'${msg}')]/span[1]`));
                browser.wait(EC.visibilityOf(el), 5000)
            });
        }).catch('Erro');
    },

    validIsPresentList: function(name) {
        element.all(by.xpath('.//*[@id="app"]/div/div/div[2]/div/ul/li/div[1]/div/span[1]')).then(function (rows) {
            return rows.map(function (row, index) {
                return row.getText().then(function (dateText) {
                    let result = {dateText: dateText};
                    if (dateText === name) {
                        result.index = index;
                    }
                    return result;
                });
            });
        }).then(function (item) {
            let promises = [];
            item.forEach((x) => {
                promises.push(new Promise((resolve, reject) => {
                    x.then((value) => {
                        resolve(value);
                    });
                }));
            });
            Promise.all(promises).then((values) => {
                // Se o item for adicionado, o array é preenchido
                let scA = values.filter((item) => {
                    return item.index;
                });
                if (scA.length) {
                    console.log(`${name} está presente na listagem`);
                } else {
                    console.log(`${name} não está presente na listagem`);
                }
            }).catch((err) => {
                console.log('Erro durante a execução: ', err);
            });
        });
    }

};
