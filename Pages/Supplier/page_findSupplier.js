/**
 * Created by roque on 15/08/17.
 */

let page_findSupplier = function(){
    /**
     * Validações de Tela AllSuppliers
     */
    this.waitScreenAllSuppliers = function(){
        var EC = protractor.ExpectedConditions;
        let searchSuppliersName = element(by.id('supplier-input-search'));
        browser.driver.wait(function () {
            browser.wait(EC.visibilityOf(searchSuppliersName), 10000);
            return searchSuppliersName;
        });
    };
    this.validMenssage = function(msg){
        let menssage = element(by.id('message')).getText();
        expect(menssage).toBe(msg);
    };
    /**
     * Interação com Botões presentes em Tela
     */
    this.clickButtonPlus = function(){
        element(by.id('')).click();
    };
    this.clickLineEditSuppliers = function(suppliers){
        element.all(by.xpath('.//*[@id="app"]/div/div[2]/ul/li/a/div/div/div[2]/span[1]')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === suppliers){
                        console.log( { searchSuppliers: dateText, suppliers: suppliers, index: index});
                        return index;
                    }
                });
            });
        }).then(function(item){
            item.forEach(function(x) {
                x.then(function(value){
                    if(value>=0){
                        console.log({Value:value});
                        element(by.id('supplier-edit-'+value)).click();
                    }
                });
            });
        });
    };
    this.clickLineDeleteSuppliers = function(suppliers){
        element.all(by.xpath('.//*[@id="app"]/div/div[2]/ul/li/a/div/div/div[2]/span[1]')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === suppliers){
                        console.log( { searchSuppliers: dateText, suppliers: suppliers, index: index});
                        return index ;
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        console.log({Value:value});
                        element(by.id('product-delete-'+value)).click();
                    }
                });
            })
        });
    };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterSuppliersSearch = function(suppliers){
        let suppliersSearch = element(by.id('supplier-input-search'));
        suppliersSearch.sendKeys(suppliers);
    };
};
module.exports = new page_findSupplier();