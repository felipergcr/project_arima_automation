/**
 * Created by roque on 15/08/17.
 */

let page_newSupplier = function(){
    /**
     * Validações de Tela AddSupplier
     */
    this.waitScreenAddSupplier = function(){
        var EC = protractor.ExpectedConditions;
        let suppliersName = element(by.id('supplier-name'));
        browser.driver.wait(function () {
            browser.wait(EC.visibilityOf(suppliersName), 10000);
            return suppliersName;
        });
    };
    this.validMenssage = function(msg){
        let menssage = element(by.id('message')).getText();
        expect(menssage).toBe(msg);
    };
    /**
     * Interação com Botões presentes em Tela
     */
    this.clickButtonRegister = function(){
        element(by.id('btn-register')).click();
    };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldSupplierName = function(supplierName){
        element(by.id('supplier-name')).clear();
        element(by.id('supplier-name')).sendKeys(supplierName);
    };
    this.enterFieldSupplierEmail = function(supplierEmail){
        element(by.id('supplier-email')).clear();
        element(by.id('supplier-email')).sendKeys(supplierEmail);
    };
    this.enterFieldSupplierPhone = function(supplierPhone){
        element(by.id('supplier-phone')).clear();
        element(by.id('supplier-phone')).sendKeys(supplierPhone);
    };
    /*this.enterFieldSupplierLogo = function(supplierLogo){
     element(by.id('supplier-thumbnail-name')).sendKeys(supplierLogo);
    };*/
    this.enterFieldSupplierDescription = function(supplierDescription){
        element(by.id('supplier-description')).clear();
        element(by.id('supplier-description')).sendKeys(supplierDescription);
    };
};
module.exports = new page_newSupplier();