/**
 * Created by roque on 16/08/17.
 */

let page_login = function(){
    /**
     * Validações de Tela Login
     */
    this.waitScreenLogin = function(){

        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let login = element(by.id('login-email'));
            browser.wait(EC.visibilityOf(login), 5000);
        }).catch('Erro');
    };
    // this.validMenssage = function(msg){
    //     let menssage = element(by.id('message')).getText();
    //     expect(menssage).toBe(msg);
    // };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldEmail = function(email){
        element(by.xpath('.//*[@id="login-email"]/input')).clear();
        element(by.xpath('.//*[@id="login-email"]/input')).sendKeys(email);
    };
    this.enterFieldPassword = function(password){
        element(by.xpath('.//*[@id="login-password"]/input')).clear();
        element(by.xpath('.//*[@id="login-password"]/input')).sendKeys(password);
    };
    /**
     * Interação com Botões presentes em Tela
     */
    this.clickButtonLogin = function(){
        element(by.id('login-btn')).click();
    };
};
module.exports = new page_login();