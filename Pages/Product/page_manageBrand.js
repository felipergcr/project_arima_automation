/**
 * Created by roque on 14/09/17.
 */

page_manageBrand = function(){
    /**
     * Validações de Tela manageBrands
     */
    this.waitButtonDelete = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let button = element(by.xpath('//button[@type="button" and contains(., "Delete")]'));
            browser.wait(EC.visibilityOf(button), 5000);
        }).catch('Erro aqui');
    };

    this.waitScreenProductsBrands = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let product = element(by.id('brand-form-name'));
            browser.wait(EC.visibilityOf(product), 5000);
        }).catch('Erro');
    };
    this.waitValidMenssage = function(msg){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            element.all(by.id('message')).then(function(row){
                return row.map(function(element){
                    return element.getText().then(function(dateText){
                        if(dateText === msg){
                            console.log( { dateText: dateText, msg: msg});
                            return dateText
                        }else{
                            console.log('Erro: Mensagem esperada inválida');
                        }
                    });
                });
            }).then(function() {
                let el = element(by.id('message'));
                browser.wait(EC.visibilityOf(el), 5000)
            });
        }).catch('Erro');
    };
    /**
     * Interação com Botões presentes em Tela
     */
    this.deleteBrandLine = function(brandName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div[2]/div/ul/li/div/div/span')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if((dateText === brandName)){
                        if(index === 0){
                            return index ;
                        } else {
                            index = index/2;
                            return index;
                        }
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.xpath(`.//*[@id="product-brand-delete-${value}"]`)).click();
                    }
                });
            })
        });
    };
    this.editBrandLine = function(brandName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div[2]/div/ul/li/div/div/span')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === brandName){
                        if(index === 0){
                            return index ;
                        } else {
                            index = index/2;
                            return index;
                        }
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.xpath(`.//*[@id="product-brand-edit-${value}"]`)).click();
                    }
                });
            })
        });
    };
    this.clickButtonRegister = function(){
        element(by.xpath('//button[contains(.,"register")]')).click();
    };
    this.clickButtonSave = function(){
        element(by.xpath('//button[contains(.,"save")]')).click();
    };
    this.confirmeDeleteBrand = function(){
        element(by.xpath('//button[@type="button" and contains(., "Delete")]')).click();

    };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldBrandName = function(brandName){
        element(by.id('brand-form-name')).clear();
        element(by.id('brand-form-name')).sendKeys(brandName);
    };
    this.enterFieldBrandDescription = function(brandDescription){
        element(by.xpath('.//*[@id="brand-form-description"]')).clear();
        element(by.xpath('.//*[@id="brand-form-description"]')).sendKeys(brandDescription);
    };
};
module.exports = new page_manageBrand();