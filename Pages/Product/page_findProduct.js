/**
 * Created by roque on 10/08/17.
 */

let page_findProduct = function(){
    /**
     * Validações de Tela AllProduct
     */
    this.validFieldChangeProductName = function(productName){
        element.all(by.xpath('.//*[@id="product-listitem-0"]/td[1]/div')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === productName){
                        console.log( { dateText: dateText, productName: productName});
                        return productName ;
                    } else {
                        expect(dateText).toBe(productName);
                    }
                });
            });
        })
    };
    this.validFieldChangeProductCategory = function(productCategory){
        element.all(by.xpath('.//*[@id="product-listitem-0"]/td[5]/div')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === productCategory){
                        console.log( { dateText: dateText, productCategory: productCategory});
                        return productCategory ;
                    } else {
                        expect(dateText).toBe(productCategory);
                    }
                });
            });
        })
    };
    this.validFieldChangeProductSupplier = function(productSupplier){
        element.all(by.xpath('.//*[@id="product-listitem-0"]/td[6]/div')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === productSupplier){
                        console.log( { dateText: dateText, productSupplier: productSupplier});
                        return productSupplier ;
                    } else {
                        expect(dateText).toBe(productSupplier);
                    }
                });
            });
        })
    };
    this.validFieldChangeProductBrand = function(productBrand){
        element.all(by.xpath('.//*[@id="product-listitem-0"]/td[7]/div')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === productBrand){
                        console.log( { dateText: dateText, productBrand: productBrand});
                        return productBrand ;
                    } else {
                        expect(dateText).toBe(productBrand);
                    }
                });
            });
        })
    };
    this.validFieldChangeProductBarcode = function(productBarcode){
        element.all(by.xpath('.//*[@id="product-listitem-0"]/td[4]/div')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === productBarcode){
                        console.log( { dateText: dateText, productBarcode: productBarcode});
                        return productBarcode ;
                    } else {
                        expect(dateText).toBe(productBarcode);
                    }
                });
            });
        })
    };
    this.waitScreenShowFilters = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let filter = element(by.id('btn-showFilter'));
            browser.wait(EC.visibilityOf(filter), 5000);
        }).catch('Erro');
    };
    this.waitScreenFieldLup = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let lup = element(by.xpath('.//*[@id="product-detail-0"]'));
            browser.wait(EC.visibilityOf(lup), 5000);
        }).catch('Erro');
    };

    this.validMenssage = function(msg){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            element.all(by.xpath('.//*[@id="message"]/div/div/span')).then(function(row){
                return row.map(function(element){
                    return element.getText().then(function(text){
                        if(text === msg){
                            return text
                        } else {
                            expect(text).toBe(msg);
                            throw new Error('Mensagem inválida');
                        }
                    });
                });
            }).then(function() {
                let el = element(by.id('message'));
                browser.wait(EC.visibilityOf(el), 5000)
            });
        }).catch('Erro');
    };
    /**
     * Interação com Botões presentes em Tela
     */
    this.clickButtonShowFilter = function(){
        element(by.id('btn-showFilter')).click();
    };
    this.clickButtonRegister = function(){
        element(by.id('btn-register')).click();
    };
    this.clickButtonSearch = function(){
        element(by.id('btn-product-search')).click();
    };
    this.clickLineViewProduct = function(productName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div/div/div[1]/table/tbody/tr/td[1]')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === productName){
                        return index;
                    }
                });
            });
        }).then(function(item){
            item.forEach(function(x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.id('product-detail-'+value)).click();
                    }
                });
            });
        });
    };
    this.clickLineEditProduct = function(productName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div/div/div[1]/table/tbody/tr/td[1]')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === productName){
                        return index;
                    }
                });
            });
        }).then(function(item){
            item.forEach(function(x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.id('product-edit-'+value)).click();
                    }
                });
            });
        });
    };
    this.clickLineDeleteProduct = function(productName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div/div/div[1]/table/tbody/tr/td[1]')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === productName){
                        console.log( { searchproduct: dateText, productName: productName, index: index});
                        return index ;
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        console.log({Value:value});
                        element(by.id('product-entry-delete-'+value)).click();
                    }
                });
            })
        });
    };

    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldProductBarcodeSearch = function(productBarcode){
        element(by.xpath('.//*[@id="productPartNumberBarCode-input-search"]')).clear();
        element(by.xpath('.//*[@id="productPartNumberBarCode-input-search"]')).sendKeys(productBarcode);
    };
    this.enterFieldProductNameSearch = function(productName){
        element(by.xpath('.//*[@id="productName-input-search"]')).clear();
       element(by.xpath('.//*[@id="productName-input-search"]')).sendKeys(productName);
    };
    this.enterFieldProductCategorySearch = function(category){
        element(by.xpath('.//*[@id="product-category"]/div/input')).sendKeys(category);
        element(by.xpath('.//*[@id="product-category"]/ul/li/a')).click();
    };
    this.enterFieldProductSupplierSearch = function(supplier){
        element(by.xpath('.//*[@id="product-supplier"]/div/input')).sendKeys(supplier);
        element(by.xpath('.//*[@id="product-supplier"]/ul/li/a')).click();
    };
    this.enterFieldProductBrandSearch = function(brand){
        element(by.xpath('.//*[@id="product-brand"]/div/input')).sendKeys(brand);
        element(by.xpath('.//*[@id="product-brand"]/ul/li/a')).click();
    };

    this.confirmeDeleteProduct = function(){
        let el = element.all(by.xpath('//button[@type="button" and contains(., "Delete")]')).filter(function (button) {
            return button.isDisplayed().then(function (isDisplayed) {
                return isDisplayed;
            });
        });
        el.click();
    };
};
module.exports = new page_findProduct();