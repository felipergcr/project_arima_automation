/**
 * Created by roque on 13/09/17.
 */

page_editProduct = function(){
    /**
     * Validações de Tela EditProduct
     */
    this.validDisabledScreenProductEdit = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let product = element(by.id('product-name'));
            expect(product.isEnabled()).toBe(false);
        }).catch('Erro');
    };
    this.waitScreenFieldProductName = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let product = element(by.id('product-name'));
            browser.wait(EC.visibilityOf(product), 5000);
        }).catch('Erro');
    };
    this.waitValidMenssage = function(msg){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            element.all(by.xpath('.//*[@id="message"]/div/div/span')).then(function(row){
                return row.map(function(element){
                    return element.getText().then(function(text){
                        if(text === msg){
                            return text
                        } else {
                            expect(text).toBe(msg);
                            throw new Error('Mensagem inválida');
                        }
                    });
                });
            }).then(function() {
                let el = element(by.id('message'));
                browser.wait(EC.visibilityOf(el), 5000)
            });
        }).catch('Erro');
    };
    /**
     * Interação com Botões presentes em Tela
     */
    this.clickButtonSave = function(){
        element(by.id('btn-save')).click();
    };
    this.clickButtonPlus = function(){
        element(by.xpath('.//*[@id="btn-edit"]')).click();
    };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldProductName = function(productName){
        element(by.xpath('.//*[@id="product-name"]')).clear();
        element(by.xpath('.//*[@id="product-name"]')).sendKeys(productName);
    };
    this.enterFieldPartNumber = function(partNumber){
        element(by.xpath('.//*[@id="product-part_number"]')).clear();
        element(by.xpath('.//*[@id="product-part_number"]')).sendKeys(partNumber);
    };
    this.enterFieldProductDescription = function(productDescription){
        element(by.xpath('.//*[@id="product-description"]')).clear();
        element(by.xpath('.//*[@id="product-description"]')).sendKeys(productDescription);
    };
    this.enterFieldProductUnitMeasures = function(measures){
        element(by.xpath('.//*[@id="add-product"]/div/div[4]/div/div/div/span')).click();
        let items = browser.findElements(by.xpath(`//div[contains(.,'${measures}')]/span`));
        items.then(function (rows) {
            rows.forEach(function (item) {
                item.getText().then(function (text) {
                    if (text === measures){
                        input = true;
                        item.click();
                    }
                });
            });
        });
    };
    this.enterFieldProductCategory = function(category){
        element(by.xpath('.//*[@id="product-category"]/div/input')).sendKeys(category);
        element(by.xpath('.//*[@id="product-category"]/ul/li/a')).click();
    };
    this.enterFieldProductSupplier = function(supplier){
        element(by.xpath('.//*[@id="product-supplier"]/div/input')).sendKeys(supplier);
        element(by.xpath('.//*[@id="product-supplier"]/ul/li/a')).click();
    };
    this.enterFieldProductBrand = function(brand){
        element(by.xpath('.//*[@id="product-brand"]/div/input')).sendKeys(brand);
        element(by.xpath('.//*[@id="product-brand"]/ul/li/a')).click();
    };
    this.enterFieldBarcode = function(barcode){
        element(by.xpath('.//*[@id="product-barCode"]')).clear();
        element(by.xpath('.//*[@id="product-barCode"]')).sendKeys(barcode);
    };

};
module.exports = new page_editProduct();