/**
 * Created by roque on 13/09/17.
 */

let page_manageCategories = function () {
    /**
     * Validações de Tela manageCategories
     */
    this.waitScreenProductsCategory = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let product = element(by.id('category-form-name'));
            browser.wait(EC.visibilityOf(product), 5000);
        }).catch('Erro');
    };
    this.waitButtonDelete = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let button = element(by.xpath('//button[@type="button" and contains(., "Delete")]'));
            browser.wait(EC.visibilityOf(button), 5000);
        }).catch('Erro aqui');
    };
    /**
     * Interação com Botões presentes em Tela
     */
    this.confirmeDeleteCategory = function(){
        element(by.xpath('//button[@type="button" and contains(., "Delete")]')).click();

    };
    this.deleteCategoryLine = function(categoryName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div[2]/div/ul/li/div/div/span')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === categoryName) {
                        if(index === 0){
                            return index ;
                        } else {
                            index = index/2;
                            return index;
                        }
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        console.log({Value:value});
                        element(by.id('product-category-delete-'+value)).click();
                    }
                });
            })
        });
    };
    this.editCategoryLine = function(categoryName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div[2]/div/ul/li/div/div/span')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === categoryName){
                        if(index === 0){
                            return index ;
                        } else {
                            index = index/2;
                            return index;
                        }
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.id('product-category-edit-'+value)).click();
                    }
                });
            })
        });
    };
    this.clickButtonRegister = function(){
        element(by.xpath('//button[contains(.,"register")]')).click();
    };
    this.clickButtonSave = function(){
        element(by.xpath('//button[contains(.,"save")]')).click();
    };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldCategoryName = function(categoryName){
        element(by.id('category-form-name')).clear();
        element(by.id('category-form-name')).sendKeys(categoryName);
    };
    this.enterFieldDescriptionName = function(descriptionName){
        element(by.xpath('.//*[@id="category-form-description"]')).clear();
        element(by.xpath('.//*[@id="category-form-description"]')).sendKeys(descriptionName);
    };
};
module.exports = new page_manageCategories();