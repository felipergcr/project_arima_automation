/**
 * Created by roque on 03/11/17.
 */
/**
 * Created by roque on 14/09/17.
 */

page_manageUnit = function(){
    /**
     * Validações de Tela manageUnit
     */
    this.waitButtonDelete = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let button = element(by.xpath('//button[@type="button" and contains(., "Delete")]'));
            browser.wait(EC.visibilityOf(button), 5000);
        }).catch('Erro aqui');
    };

    this.waitScreenManagerUnit = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let product = element(by.id('unit-form-name'));
            browser.wait(EC.visibilityOf(product), 5000);
        }).catch('Erro');
    };

    /**
     * Interação com Botões presentes em Tela
     */
    this.deleteUnitLine = function(UnitName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div[2]/div/ul/li/div/div/span')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if((dateText === UnitName)){
                        if(index === 0){
                            return index ;
                        } else {
                            index = index/2;
                            return index;
                        }
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.xpath(`.//*[@id="product-unit-delete-${value}"]`)).click();
                    }
                });
            })
        });
    };
    this.editUnitLine = function(UnitName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div[2]/div/ul/li/div/div/span')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === UnitName){
                        if(index === 0){
                            return index ;
                        } else {
                            index = index/2;
                            return index;
                        }
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.xpath(`.//*[@id="product-unit-edit-${value}"]`)).click();
                    }
                });
            })
        });
    };
    this.clickButtonRegister = function(){
        element(by.xpath('//button[contains(.,"register")]')).click();
    };
    this.clickButtonSave = function(){
        element(by.xpath('//button[contains(.,"save")]')).click();
    };
    this.confirmeDeleteUnit = function(){
        element(by.xpath('//button[@type="button" and contains(., "Delete")]')).click();

    };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldUnitName = function(UnitName){
        element(by.id('unit-form-name')).clear();
        element(by.id('unit-form-name')).sendKeys(UnitName);
    };
    this.enterFieldUnitDescription = function(unitDescription){
        element(by.id('unit-form-description')).clear();
        element(by.id('unit-form-description')).sendKeys(unitDescription);
    };
};
module.exports = new page_manageUnit();