/**
 * Created by roque on 06/11/17.
 */

page_managerDepartment = function(){
    /**
     * Validações de Tela managerDepartments
     */
    this.waitScreenManagerDepartment = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let department = element(by.id('department-form-name'));
            browser.wait(EC.visibilityOf(department), 5000);
        }).catch('Erro');
    };
    this.waitButtonDelete = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let button = element(by.xpath('//button[@type="button" and contains(., "Delete")]'));
            browser.wait(EC.visibilityOf(button), 5000);
        }).catch('Erro aqui');
    };
    this.validIsPresentListDepartment = function(name) {
        element.all(by.xpath('.//*[@id="app"]/div/div/div[3]/div/ul/li/div/div/span')).then(function (rows) {
            return rows.map(function (row, index) {
                return row.getText().then(function (dateText) {
                    let result = {dateText: dateText};
                    if (dateText === name) {
                        result.index = index;
                    }
                    return result;
                });
            });
        }).then(function (item) {
            let promises = [];
            item.forEach((x) => {
                promises.push(new Promise((resolve, reject) => {
                    x.then((value) => {
                        resolve(value);
                    });
                }));
            });
            Promise.all(promises).then((values) => {
                // Se o item for adicionado, o array é preenchido
                let scA = values.filter((item) => {
                    return item.index;
                });
                if (scA.length) {
                    console.log(`${name} está presente na listagem`);
                } else {
                    console.log(`${name} não está presente na listagem`);
                }
            }).catch((err) => {
                console.log('Erro durante a execução: ', err);
            });
        });
    }
    /**
     * Interação com Botões presentes em Tela
     */
    this.deleteDepartmentLine = function(departmentName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div[3]/div/ul/li/div/div/span')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === departmentName) {
                        if(index === 0){
                            return index ;
                        } else {
                            index = index/2;
                            return index;
                        }
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        console.log({Value:value});
                        element(by.id('store-department-delete-'+value)).click();
                    }
                });
            })
        });
    };
    this.editDepartmentLine = function(departmentName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div[3]/div/ul/li/div/div/span')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === departmentName){
                        if(index === 0){
                            return index ;
                        } else {
                            index = index/2;
                            return index;
                        }
                    }
                });
            });
        }).then(function(item){
            item.forEach(function (x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.id('store-department-edit-'+value)).click();
                    }
                });
            })
        });
    };
    this.confirmeDeleteDepartment = function(){
        element(by.xpath('//button[@type="button" and contains(., "Delete")]')).click();

    };
    this.clickButtonRegister = function(){
        element(by.xpath('//button[contains(.,"Register")]')).click();
    };
    this.clickButtonSave = function(){
        element(by.xpath('//button[contains(.,"save")]')).click();
    };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldDepartmentName = function(departmentName){
        element(by.id('department-form-name')).clear();
        element(by.id('department-form-name')).sendKeys(departmentName);
    };
    this.enterFieldDepartmentDescription = function(departmentDescription){
        element(by.id('department-form-description')).clear();
        element(by.id('department-form-description')).sendKeys(departmentDescription);
    };

};
module.exports = new page_managerDepartment();