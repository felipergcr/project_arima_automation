/**
 * Created by roque on 06/11/17.
 */

page_newStore = function(){
    /**
     * Validações de Tela new Store
     */
    this.waitScreenNewStore = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let storeName = element(by.id('store-name'));
            browser.wait(EC.visibilityOf(storeName), 5000);
        }).catch('Erro');
    };
    /**
     * Interação com Botões presentes em Tela
     */
    this.clickButtonRegister = function(){
        element(by.id('btn-register')).click();
    };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldStoreName = function(storeName){
        element(by.id('store-name')).clear();
        element(by.id('store-name')).sendKeys(storeName);
    };
    this.enterFieldStoreDescription = function(storeDescription){
        element(by.id('store-description')).clear();
        element(by.id('store-description')).sendKeys(storeDescription);
    };
    this.enterFieldStoreTaxUnits = function(taxUnits){
        element(by.xpath('.//*[@id="add-store"]/div/div[4]/div/div/div/span')).click();
        let items = browser.findElements(by.xpath(`//div[contains(.,'${taxUnits}')]/span`));
        items.then(function (rows) {
            rows.forEach(function (item) {
                item.getText().then(function (text) {
                    if (text === taxUnits){
                        input = true;
                        item.click();
                    }
                });
            });
        });
    };
    this.enterFieldStoreCountry = function(country){
        element(by.xpath('.//*[@id="store-address-country"]/div/input')).sendKeys(country);
        element(by.xpath('.//*[@id="store-address-country"]/ul/li/a')).click();
    };
    this.enterFieldStoreState = function(storeState){
        element(by.id('store-address-state')).clear();
        element(by.id('store-address-state')).sendKeys(storeState);
    };
    this.enterFieldStoreCity = function(storeCity){
        element(by.id('store-address-city')).clear();
        element(by.id('store-address-city')).sendKeys(storeCity);
    };
};
module.exports = new page_newStore();