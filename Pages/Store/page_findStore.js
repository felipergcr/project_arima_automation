/**
 * Created by roque on 06/11/17.
 */

page_findStore = function(){
    /**
     * Validações de Tela find Store
     */
    this.validDisabledScreenStoreEdit = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let store = element(by.id('store-name'));
            expect(store.isEnabled()).toBe(false);
        }).catch('Erro');
    };
    this.waitScreenFindStore = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let store = element(by.xpath('.//*[@id="app"]/div/div/div/div/div[1]/table/thead/th[3]/div/div[1]'));
            browser.wait(EC.visibilityOf(store), 5000);
        }).catch('Erro');
    };
    this.waitScreenStoreName = function(){
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let storeName = element(by.id('store-name'));
            browser.wait(EC.visibilityOf(storeName), 5000);
        }).catch('Erro');
    };
    /**
     * Interação com Botões presentes em Tela
     */
    this.clickButtonPlus = function(){
        element(by.xpath('.//*[@id="btn-edit"]')).click();
    };
    this.clickButtonSave = function(){
        element(by.id('btn-save')).click();
    };
    this.clickLineEditStore = function(storeName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div/div/div[1]/table/tbody/tr/td[2]')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === storeName){
                        return index;
                    }
                });
            });
        }).then(function(item){
            item.forEach(function(x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.id('store-edit-'+value)).click();
                    }
                });
            });
        });
    };
    this.clickLineDeleteStore = function(storeName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div/div/div[1]/table/tbody/tr/td[2]')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === storeName){
                        return index;
                    }
                });
            });
        }).then(function(item){
            item.forEach(function(x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.id('store-entry-delete-'+value)).click();
                    }
                });
            });
        });
    };
    this.clickLineDetailStore = function(storeName){
        element.all(by.xpath('.//*[@id="app"]/div/div/div/div/div[1]/table/tbody/tr/td[2]')).then(function(rows){
            return rows.map(function(row, index){
                return row.getText().then(function(dateText){
                    if(dateText === storeName){
                        return index;
                    }
                });
            });
        }).then(function(item){
            item.forEach(function(x) {
                x.then(function(value){
                    if(value>=0){
                        element(by.id('store-detail-'+value)).click();
                    }
                });
            });
        });
    };
    this.confirmeDeleteStore = function(){
        let el = element.all(by.xpath('//button[@type="button" and contains(., "Delete")]')).filter(function (button) {
            return button.isDisplayed().then(function (isDisplayed) {
                return isDisplayed;
            });
        });
        el.click();
    };
    /**
     * Interação com Entrada de valores em Tela
     */
    this.enterFieldStoreName = function(storeName){
        element(by.id('store-name')).clear();
        element(by.id('store-name')).sendKeys(storeName);
    };
    this.enterFieldStoreDescription = function(storeDescription){
        element(by.id('store-description')).clear();
        element(by.id('store-description')).sendKeys(storeDescription);
    };
    this.enterFieldStoreTaxUnits = function(taxUnits){
        element(by.xpath('.//*[@id="edit-store"]/div/div[4]/div/div/div/span')).click();
        let items = browser.findElements(by.xpath(`//div[contains(.,'${taxUnits}')]/span`));
        items.then(function (rows) {
            rows.forEach(function (item) {
                item.getText().then(function (text) {
                    if (text === taxUnits){
                        input = true;
                        item.click();
                    }
                });
            });
        });
    };
    this.enterFieldStoreCountry = function(country){
        element(by.xpath('.//*[@id="store-address-country"]/div/input')).sendKeys(country);
        element(by.xpath('.//*[@id="store-address-country"]/ul/li/a')).click();
    };
    this.enterFieldStoreState = function(storeState){
        element(by.id('store-address-state')).clear();
        element(by.id('store-address-state')).sendKeys(storeState);
    };
    this.enterFieldStoreCity = function(storeCity){
        element(by.id('store-address-city')).clear();
        element(by.id('store-address-city')).sendKeys(storeCity);
    };
};
module.exports = new page_findStore();
