/**
 * Created by roque on 02/06/17.
 */
let page_home = function(){

    this.waitScreenDashboard = function(){

        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let dashboard = element(by.xpath('.//*[@id="header"]/div/div[1]/h2'));
            browser.wait(EC.visibilityOf(dashboard), 5000);
        }).catch('Erro');
    };
    this.clickMenu = function(){
        element(by.id('menu')).click();
    };
    /**
     *  Menu Products
     */
    this.clickMenuProducts = function(){
        element(by.id('menu-products')).click();
    };
    this.clickMenuProductsFindProducts = function(){
        element(by.id('menu-products-all-products')).click();
    };
    this.clickMenuProductsAddNew = function(){
        element(by.id('menu-products-add-new')).click();
    };
    this.clickMenuProductsManageCategory = function(){
        element(by.id('menu-products-category')).click();
    };
    this.clickMenuProductsManageBrands = function(){
        element(by.id('menu-products-brand')).click();
    };
    this.clickMenuProductsManageUnit = function(){
        element(by.id('menu-products-unit')).click();
    };
    /**
     *  Menu Suppliers
     */
    this.clickMenuSuppliers = function(){
        element(by.id('menu-suppliers')).click();
    };
    this.clickMenuSuppliersAllSuppliers = function(){
        element(by.id('menu-suppliers-all-suppliers')).click();
    };
    this.clickMenuSuppliersAddNew = function(){
        element(by.id('menu-suppliers-add-new')).click();
    };
    /**
     *  Menu Store
     */
    this.clickMenuStores = function(){
        element(by.id('menu-stores')).click();
    };
    this.clickMenuStoresNewStore = function(){
        element(by.id('menu-stores-add-new')).click();
    };
    this.clickMenuStoresFindStore = function(){
        element(by.id('menu-stores-all-stores')).click();
    };
    this.clickMenuStoresManagerDepartment = function(){
        element(by.id('menu-stores-department')).click();
    };

    /**
     *  Menu HallHack
     */
    this.clickMenuHallHack = function(){
        element(by.id('menu-hall')).click();
    };
    this.clickMenuHallHackNewHallHack = function(){
        element(by.id('menu-hall-add')).click();
    };
    this.clickMenuHallHackFindAHall = function(){
        element(by.id('menu-hall-all')).click();
    };
    /**
     *  Menu Update Labels
     */
    this.clickMenuUpdateLabels = function(){
        element(by.id('menu-filter-and-update')).click();
    };
    /**
     *  Menu User

     */
    this.clickMenuUser = function(){
        element(by.xpath('.//*[@id="header"]/div/div[2]/div[1]/ul/li[7]/button')).click();
    };
    this.clickMenuFindUser = function(){
        element(by.id('menu-users-all-users')).click();
    };
    this.clickMenuNewUser = function(){
        element(by.id('menu-users-add-new')).click();
    };
    this.logout = function(){
        element(by.xpath('.//*[@id="btn-logged"]')).click();
        element(by.xpath('.//*[@id="btn-logout-right"]/button')).click();
    };

};
module.exports = new page_home();