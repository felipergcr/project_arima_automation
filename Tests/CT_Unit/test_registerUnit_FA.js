/**
 * Created by roque on 03/11/17.
 */

describe(' - WEBMARKET - FLUXO ALTERNATIVO ', function(){
    let page_manageUnit = require('../../Pages/Product/page_manageUnit');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN MANAGER UNIT - Cadastro de unidade informando dados invalidos - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageUnit();
        page_manageUnit.enterFieldUnitName('@@@@');
        page_manageUnit.enterFieldUnitDescription('DESCRIPTION TEST');
        page_manageUnit.clickButtonRegister();
        utils.validMessageField('The name field format is invalid.');
        page_home.logout();
    });

    it('SCREEN MANAGE UNIT - Cadastro de unidade duplicado - sem sucesso' ,function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageUnit();
        page_manageUnit.enterFieldUnitName('UNIT 0');
        page_manageUnit.enterFieldUnitDescription('DESCRIPTION UNIT 0');
        page_manageUnit.clickButtonRegister();
        utils.validMessage('{ "code": 17102, "message": "Error, expected Unit of measurement name to be unique" }');
        page_home.logout();
    });
});
