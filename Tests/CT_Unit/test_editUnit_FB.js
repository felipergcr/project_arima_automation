/**
 * Created by roque on 03/11/17.
 */

describe(' - WEBMARKET - FLUXO BÁSICO ', function(){
    let page_manageUnit = require('../../Pages/Product/page_manageUnit');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN MANAGER UNIT - Edição de unidade informando dados válidos - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageUnit();
        page_manageUnit.editUnitLine('UNIT 5');
        page_manageUnit.enterFieldUnitDescription('EDITING UNIT 5');
        page_manageUnit.clickButtonSave();
        utils.validMessageField('Unit of measurement successfully updated');
        page_home.logout();
    });
});
