/**
 * Created by roque on 31/10/17.
 */

describe(' - WEBMARKET - FLUXO BÁSICO ', function(){
    let page_manageCategories = require('../../Pages/Product/page_manageCategories');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN MANAGER CATEGORIES - Cadastro de categorias informando dados válidos - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageCategory();
        let row = utils.toRegisterProductCategoryApplication('valid');
        utils.validMessageField('Category successfully created');
        utils.validIsPresentList(row.inputValueCategoryName);
        page_home.logout();
    });

    it('SCREEN MANAGER CATEGORIES - Deletar categoria válida - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageCategory();
        page_manageCategories.deleteCategoryLine('CATEGORY 3');
        page_manageCategories.waitButtonDelete();
        page_manageCategories.confirmeDeleteCategory();
        utils.validMessageField('Category successfully removed');
        utils.validIsPresentList('CATEGORY 3');
        page_home.logout();
    });
});