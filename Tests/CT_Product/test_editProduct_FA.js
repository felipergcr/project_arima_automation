/**
 * Created by roque on 31/10/17.
 */

describe(' - WEBMARKET - FLUXO ALTERNATIVO ', function(){
    let page_findProduct = require('../../Pages/Product/page_findProduct');
    let page_editProduct = require('../../Pages/Product/page_editProduct');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

   it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo nome do produto e informando um nome inválido - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 0');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineEditProduct('PRODUCT 0');
        page_editProduct.waitScreenFieldProductName();
        page_editProduct.enterFieldProductName('@@@@');
        page_editProduct.clickButtonSave();
        utils.validMessageField('The name field format is invalid.');
        page_home.logout();
    });
    it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo nome do produto e informando um part number inválido - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 5');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineEditProduct('PRODUCT 5');
        page_editProduct.waitScreenFieldProductName();
        page_editProduct.enterFieldPartNumber('@@@@');
        page_editProduct.clickButtonSave();
        utils.validMessageField('The partNumber field may only contain numeric characters.');
        page_home.logout();
    });
    it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo nome do produto e informando um Barcode inválido - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 4');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineEditProduct('PRODUCT 4');
        page_editProduct.waitScreenFieldProductName();
        page_editProduct.enterFieldBarcode('@@@@');
        page_editProduct.clickButtonSave();
        utils.validMessageField('The barCode field may only contain numeric characters.');
        page_home.logout();
    });
    it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo nome do produto, e alterando o produto pela ação de visualizar informando o campo name inválido - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 4');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineViewProduct('PRODUCT 4');
        page_editProduct.validDisabledScreenProductEdit();
        page_editProduct.clickButtonPlus();
        page_editProduct.enterFieldProductName('@@@@');
        page_editProduct.clickButtonSave();
        utils.validMessageField('The name field format is invalid.');
        page_home.logout();
    });

});