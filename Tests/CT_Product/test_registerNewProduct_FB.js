/**
 * Created by roque on 30/10/17.
 */

describe(' - WEBMARKET - FLUXO BÁSICO ', function(){
    let page_newProduct = require('../../Pages/Product/page_newProduct');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });


    it('SCREEN NEW PRODUCT - Cadastro de produto informando dados válidos - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsAddNew();
        let row = utils.toRegisterProductApplication('hard');
        utils.validMessageField('Product successfully created');
        page_home.logout();
    });

    it('SCREEN NEW PRODUCT - Cadastro de produto informando somente supplier - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsAddNew();
        let row = utils.toRegisterProductApplication('supplier');
        utils.validMessageField('Product successfully created');
        page_home.logout();
    });

    it('SCREEN NEW PRODUCT - Cadastro de produto informando somente brand - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsAddNew();
        let row = utils.toRegisterProductApplication('brand');
        utils.validMessageField('Product successfully created');
        page_home.logout();
    });

    it('SCREEN NEW PRODUCT - Cadastro de produto informando somente category - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsAddNew();
        let row = utils.toRegisterProductApplication('category');
        utils.validMessageField('Product successfully created');
        page_home.logout();
    });

    it('SCREEN NEW PRODUCT - Cadastro de produto sem informar supplier, brands e category - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsAddNew();
        let row = utils.toRegisterProductApplication('none');
        utils.validMessage('Product successfully created');
        page_home.logout();
    });
});