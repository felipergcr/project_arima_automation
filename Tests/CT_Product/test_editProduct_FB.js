/**
 * Created by roque on 31/10/17.
 */

describe(' - WEBMARKET - FLUXO BÁSICO ', function(){
    let page_findProduct = require('../../Pages/Product/page_findProduct');
    let page_editProduct = require('../../Pages/Product/page_editProduct');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo nome do produto e alterando a categoria - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 0');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineEditProduct('PRODUCT 0');
        page_editProduct.waitScreenFieldProductName();
        page_editProduct.enterFieldProductCategory('CATEGORY 1');
        page_editProduct.clickButtonSave();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 0');
        page_findProduct.enterFieldProductCategorySearch('CATEGORY 1');
        page_findProduct.clickButtonSearch();
        page_findProduct.validFieldChangeProductName('PRODUCT 0');
        page_findProduct.validFieldChangeProductCategory('CATEGORY 1');
        page_home.logout();
    });

    it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo nome do produto e alterando o supplier - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 1');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineEditProduct('PRODUCT 1');
        page_editProduct.waitScreenFieldProductName();
        page_editProduct.enterFieldProductSupplier('SUPPLIER 0');
        page_editProduct.clickButtonSave();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 1');
        page_findProduct.enterFieldProductSupplierSearch('SUPPLIER 0');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.validFieldChangeProductName('PRODUCT 1');
        page_findProduct.validFieldChangeProductSupplier('SUPPLIER 0');
        page_home.logout();
    });

    it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo nome do produto e alterando a brand - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 2');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineEditProduct('PRODUCT 2');
        page_editProduct.waitScreenFieldProductName();
        page_editProduct.enterFieldProductBrand('BRAND 0');
        page_editProduct.clickButtonSave();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 2');
        page_findProduct.enterFieldProductBrandSearch('BRAND 0');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.validFieldChangeProductName('PRODUCT 2');
        page_findProduct.validFieldChangeProductBrand('BRAND 0');
        page_home.logout();
    });

    it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo nome do produto e alterando a categoria, supplier e brand - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 3');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineEditProduct('PRODUCT 3');
        page_editProduct.waitScreenFieldProductName();
        page_editProduct.enterFieldProductCategory('CATEGORY 1');
        page_editProduct.enterFieldProductSupplier('SUPPLIER 1');
        page_editProduct.enterFieldProductBrand('BRAND 1');
        page_editProduct.clickButtonSave();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 3');
        page_findProduct.enterFieldProductCategorySearch('CATEGORY 1');
        page_findProduct.enterFieldProductSupplierSearch('SUPPLIER 1');
        page_findProduct.enterFieldProductBrandSearch('BRAND 1');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.validFieldChangeProductName('PRODUCT 3');
        page_findProduct.validFieldChangeProductCategory('CATEGORY 1');
        page_findProduct.validFieldChangeProductSupplier('SUPPLIER 1');
        page_findProduct.validFieldChangeProductBrand('BRAND 1');
        page_home.logout();
    });

    it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo barcode - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('PRODUCT 4');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineEditProduct('PRODUCT 4');
        page_editProduct.waitScreenFieldProductName();
        page_editProduct.enterFieldBarcode('10101010');
        page_editProduct.enterFieldProductCategory('CATEGORY 1');
        page_editProduct.enterFieldProductSupplier('SUPPLIER 1');
        page_editProduct.enterFieldProductBrand('BRAND 1');
        page_editProduct.clickButtonSave();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductBarcodeSearch('10101010');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.validFieldChangeProductName('PRODUCT 4');
        page_findProduct.validFieldChangeProductCategory('CATEGORY 1');
        page_findProduct.validFieldChangeProductSupplier('SUPPLIER 1');
        page_findProduct.validFieldChangeProductBrand('BRAND 1');
        page_findProduct.validFieldChangeProductBarcode('10101010');
        page_home.logout();
    });

    it('SCREEN FIND PRODUCT - Edição de produto pesquisando por uma parte do nome do produto - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('9');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineEditProduct('PRODUCT 9');
        page_editProduct.waitScreenFieldProductName();
        page_editProduct.enterFieldBarcode('20202020');
        page_editProduct.enterFieldProductCategory('CATEGORY 3');
        page_editProduct.clickButtonSave();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductBarcodeSearch('20202020');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.validFieldChangeProductName('PRODUCT 9');
        page_findProduct.validFieldChangeProductCategory('CATEGORY 3');
        page_findProduct.validFieldChangeProductBarcode('20202020');
        page_home.logout();
    });
    it('SCREEN FIND PRODUCT - Edição de produto pesquisando pelo nome do produtoe, e alterando o produto pela ação de visualizar informando o campo barcode válido - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('9');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineViewProduct('PRODUCT 9');
        page_editProduct.validDisabledScreenProductEdit();
        page_editProduct.clickButtonPlus();
        page_editProduct.enterFieldBarcode('30303030');
        page_editProduct.enterFieldProductCategory('CATEGORY 5');
        page_editProduct.clickButtonSave();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductBarcodeSearch('30303030');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.validFieldChangeProductName('PRODUCT 9');
        page_findProduct.validFieldChangeProductCategory('CATEGORY 5');
        page_findProduct.validFieldChangeProductBarcode('30303030');
        page_home.logout();
    });

    it('SCREEN FIND PRODUCT - Exclusão de produto pesquisando pelo nome do produto - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsFindProducts();
        page_findProduct.waitScreenShowFilters();
        page_findProduct.clickButtonShowFilter();
        page_findProduct.enterFieldProductNameSearch('9');
        page_findProduct.clickButtonSearch();
        page_findProduct.waitScreenFieldLup();
        page_findProduct.clickLineDeleteProduct('PRODUCT 9');
        page_findProduct.confirmeDeleteProduct();
        utils.validMessage('Product successfully removed');
        page_home.logout();
    });

});