/**
 * Created by roque on 31/10/17.
 */

describe(' - WEBMARKET - FLUXO ALTERNATIVO ', function(){
    let page_newProduct = require('../../Pages/Product/page_newProduct');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN NEW PRODUCT - Cadastro de produto informando o name com dados inválidos - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsAddNew();
        utils.toRegisterProductInvalidApplication('name');
        utils.validMessageField('The name field format is invalid.');
        page_home.logout();
    });

    it('SCREEN NEW PRODUCT - Cadastro de produto informando o part number com dados inválidos - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsAddNew();
        utils.toRegisterProductInvalidApplication('partNumber');
        utils.validMessageField('The partNumber field may only contain numeric characters.');
        page_home.logout();
    });

    it('SCREEN NEW PRODUCT - Cadastro de produto informando o Barcode com dados inválidos - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsAddNew();
        utils.toRegisterProductInvalidApplication('barcode');
        utils.validMessageField('The barCode field may only contain numeric characters.');
        page_home.logout();
    });

    it('SCREEN NEW PRODUCT - Cadastro de produto informando produto duplicado - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsAddNew();
        utils.toRegisterProductInvalidApplication('PRODUCT 0');
        utils.validMessage('{ "code": 2107, "message": "Error, expected name to be unique" }');
        page_home.logout();
    });

});