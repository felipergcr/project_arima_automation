/**
 * Created by roque on 06/11/17.
 */

describe(' - WEBMARKET - FLUXO BÁSICO ', function(){
    let page_managerDepartment = require('../../Pages/Store/page_managerDepartment');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN MANAGER DEPARTMENT - Edição de departamento informando dados válidos - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuStores();
        page_home.clickMenuStoresManagerDepartment();
        page_managerDepartment.waitScreenManagerDepartment();
        page_managerDepartment.editDepartmentLine('DEPARTMENT 6');
        page_managerDepartment.enterFieldDepartmentDescription('EDITTING DEPARTMENT 6');
        page_managerDepartment.clickButtonSave();
        utils.validMessage('Department successfully updated');
        page_managerDepartment.validIsPresentListDepartment('DEPARTMENT 6');
        page_home.logout();
    });
});