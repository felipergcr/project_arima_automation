/**
 * Created by roque on 14/09/17.
 */
describe(' - WEBMARKET - FLUXO BÁSICO ', function(){
    let page_manageBrand = require('../../Pages/Product/page_manageBrand');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN MANAGE BRANDS - Cadastro de brands informando dados válidos - com sucesso' ,function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageBrands();
        let row = utils.toRegisterProductBrandApplication('valid');
        utils.validMessage('Brand successfully created');
        utils.validIsPresentList(row.inputValueBrandName);
    });

    it('SCREEN MANAGE BRANDS - Deletar brand válida - com sucesso' ,function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageBrands();
        page_manageBrand.deleteBrandLine('BRAND 4');
        page_manageBrand.waitButtonDelete();
        page_manageBrand.confirmeDeleteBrand();
        utils.validMessage('Brand successfully removed');
        utils.validIsPresentList('BRAND 4');
        page_home.logout();
    });
});