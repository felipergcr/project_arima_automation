/**
 * Created by roque on 03/11/17.
 */

describe(' - WEBMARKET - FLUXO ALTERNATIVO ', function(){
    let page_manageBrand = require('../../Pages/Product/page_manageBrand');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN MANAGER BRAND - Edição de brand informando dados inválidos - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageBrands();
        page_manageBrand.editBrandLine('BRAND 4');
        page_manageBrand.enterFieldBrandName('@@@ 4');
        page_manageBrand.enterFieldBrandDescription('EDITING BRAND 4');
        page_manageBrand.clickButtonSave();
        utils.validMessageField('The name field format is invalid.');
        page_home.logout();
    });
});
