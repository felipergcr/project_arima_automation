/**
 * Created by roque on 14/09/17.
 */
describe(' - WEBMARKET - FLUXO ALTERNATIVO ', function(){

    let page_manageBrand = require('../../Pages/Product/page_manageBrand');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN MANAGE BRANDS - Cadastro de brands informando dados inválidos - sem sucesso' ,function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageBrands();
        page_manageBrand.enterFieldBrandName('@@@');
        utils.validMessageField('The name field format is invalid.');
        page_home.logout();
    });

    it('SCREEN MANAGE BRANDS - Cadastro de brand duplicado - sem sucesso' ,function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuProducts();
        page_home.clickMenuProductsManageBrands();
        page_manageBrand.enterFieldBrandName('BRAND 0');
        page_manageBrand.enterFieldBrandDescription('DESCRIPTION BRAND 0');
        page_manageBrand.clickButtonRegister();
        utils.validMessage('{ "code": 10102, "message": "Error, expected name to be unique" }');
    });
});