/**
 * Created by roque on 06/11/17.
 */

describe(' - WEBMARKET - FLUXO ALTERNATIVO ', function(){
    let page_newStore = require('../../Pages/Store/page_newStore');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN NEW STORE - Cadastro de store informando name invalido - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuStores();
        page_home.clickMenuStoresNewStore();
        page_newStore.waitScreenNewStore();
        utils.toRegisterStoreApplication('invalidName');
        utils.validMessageField('The name field format is invalid.');
        page_home.logout();
    });
});