/**
 * Created by roque on 06/11/17.
 */

describe(' - WEBMARKET - FLUXO ALTERNATIVO ', function(){
    let page_findStore = require('../../Pages/Store/page_findStore');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN FIND STORE - Edição de store informando dados inválidos - sem sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuStores();
        page_home.clickMenuStoresFindStore();
        page_findStore.waitScreenFindStore();
        page_findStore.clickLineEditStore('STORE 2');
        page_findStore.waitScreenStoreName();
        page_findStore.enterFieldStoreName('@@@@');
        page_findStore.enterFieldStoreTaxUnits('R$');
        page_findStore.enterFieldStoreState('Pernambuco');
        page_findStore.enterFieldStoreCity('Recife');
        page_findStore.clickButtonSave();
        utils.validMessageField('The name field format is invalid.');
    });
});