/**
 * Created by roque on 06/11/17.
 */

describe(' - WEBMARKET - FLUXO BÁSICO ', function(){
    let page_findStore = require('../../Pages/Store/page_findStore');
    let page_home = require('../../Pages/Home/page_home.js');
    let utils = require('../../utils.js');
    var originalTimeout;

    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get(utils.path() + ':8080/#/');
            done();
        }, 100);
    });

    afterEach(function(){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it('SCREEN FIND STORE - Edição de store informando dados válidos - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuStores();
        page_home.clickMenuStoresFindStore();
        page_findStore.waitScreenFindStore();
        page_findStore.clickLineEditStore('STORE 1');
        page_findStore.waitScreenStoreName();
        page_findStore.enterFieldStoreDescription('EDITTING STORE 1 DESCRIPTION');
        page_findStore.enterFieldStoreTaxUnits('R$');
        page_findStore.enterFieldStoreState('Pernambuco');
        page_findStore.enterFieldStoreCity('Recife');
        page_findStore.clickButtonSave();
        utils.validMessage('store successfully updated');
    });

    it('SCREEN FIND STORE - Exclusão de store válida - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuStores();
        page_home.clickMenuStoresFindStore();
        page_findStore.waitScreenFindStore();
        page_findStore.clickLineDeleteStore('STORE 5');
        page_findStore.confirmeDeleteStore();
        utils.validMessage('store successfully removed');
    });
    it('SCREEN FIND STORE - Edição de store alterando pela ação de visualizar store - com sucesso', function(){
        utils.loginInToApplication();
        page_home.clickMenu();
        page_home.clickMenuStores();
        page_home.clickMenuStoresFindStore();
        page_findStore.waitScreenFindStore();
        page_findStore.clickLineDetailStore('STORE 8');
        page_findStore.validDisabledScreenStoreEdit();
        page_findStore.clickButtonPlus();
        page_findStore.enterFieldStoreDescription('EDITTING STORE 8 DESCRIPTION BY DETAIL ACTION');
        page_findStore.enterFieldStoreTaxUnits('R$');
        page_findStore.enterFieldStoreState('Pernambuco');
        page_findStore.enterFieldStoreCity('Recife');
        page_findStore.clickButtonSave();
        utils.validMessage('store successfully updated');
    });
});