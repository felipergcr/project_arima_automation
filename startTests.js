var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');

var reporter = new HtmlScreenshotReporter({
    dest: 'ReportJasmine',
    showSummary: true,
    showQuickLinks: true,
    showConfiguration: true,
    screenshotsFolder: 'images',
    takeScreenshots: true,
    takeScreenshotsOnlyOnFailures: true,
    fixedScreenshotName: true,
    ignoreSkippedSpecs: false,
    consolidate: true,
    consolidateAll: true,
    preserveDirectory: true,
    reportTitle: 'Testlauf_',
    fileNameSeparator: '_',
    fileNameDateSuffix: true,
    cleanDestination: true,
    filename: 'MyReportRoque.html'
});

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',

    highlightDelay: 200,

    multiCapabilities: [
        {'browserName': 'firefox'}
        //{'browserName': 'chrome'}

    ],
    baseUrl:'http:localhost:8080/#/',

    specs: [
        /**
         *  SCREEN PRODUCT
         */
        // 'Tests/CT_Product/test_registerNewProduct_FB.js',
        // 'Tests/CT_Product/test_registerNewProduct_FA.js',
        // 'Tests/CT_Product/test_editProduct_FB.js',
        // 'Tests/CT_Product/test_editProduct_FA.js',
        // 'Tests/CT_Category/test_registerCategory_FB.js',
        // 'Tests/CT_Category/test_registerCategory_FA.js',
        // 'Tests/CT_Category/test_editCategory_FB.js',
        // 'Tests/CT_Brand/test_registerBrand_FB.js',
        // 'Tests/CT_Brand/test_registerBrand_FA.js',
        // 'Tests/CT_Brand/test_editBrand_FA.js',
        // 'Tests/CT_Unit/test_registerUnit_FB.js',
        // 'Tests/CT_Unit/test_registerUnit_FA.js',
        // 'Tests/CT_Unit/test_editUnit_FB.js',
        // 'Tests/CT_Unit/test_editUnit_FA.js',

        /***
         * SCREEN SUPPLIER
         */


        /**
         * SCREEN STORES
         */
        // 'Tests/CT_Store/test_registerStore_FB.js',
        // 'Tests/CT_Store/test_registerStore_FA.js',
        // 'Tests/CT_Store/test_editStore_FB.js',
        // 'Tests/CT_Store/test_editStore_FA.js',
        //     'Tests/CT_Department/test_registerDepartment_FB.js',
        //     'Tests/CT_Department/test_registerDepartment_FA.js',
        //     'Tests/CT_Department/test_editDepartment_FB.js',
        //     'Tests/CT_Department/test_editDepartment_FA.js',

        /**
         * SCREEN HALL
         */
        
    ],

    framework: 'jasmine2',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 10000,
        expectationResultHandler: function(passed, assertion) {
            /**
             * only take screenshot if assertion failed
             */
            if(passed) {
                return;
            }
            browser.takeScreenshot('./Errors/assertionError_' + assertion.error.message + '.png');
        }
    },

    // A callback function called once configs are read but before any environment
    // setup. This will only run once, and before onPrepare.
    // You can specify a file containing code to run by setting beforeLaunch to
    // the filename string.
    /*beforeLaunch: function() {
        console.log('beforeLaunch');
        // browser.ignoreSynchronization = true;
        // browser.waitForAngular();
        // browser.get(utils.path() + ':8080/#/');
    },*/

    // A callback function called once protractor is ready and available, and
    // before the specs are executed.
    // If multiple capabilities are being run, this will run once per
    // capability.
    // You can specify a file containing code to run by setting onPrepare to
    // the filename string.
    /*onPrepare: function() {

    },*/

    // A callback function called once tests are finished.
    /*onComplete: function() {
        // At this point, tests will be done but global objects will still be
        // available.
    },*/

    // A callback function called once the tests have finished running and
    // the WebDriver instance has been shut down. It is passed the exit code
    // (0 if the tests passed). This is called once per capability.
    /*onCleanUp: function(exitCode) {
        console.log('onCleanUp', exitCode);
    },*/

    // A callback function called once all tests have finished running and
    // the WebDriver instance has been shut down. It is passed the exit code
    // (0 if the tests passed). This is called only once before the program
    // exits (after onCleanUp).
    afterLaunch: function() {},

    // The params object will be passed directly to the Protractor instance,
    // and can be accessed from your test as browser.params. It is an arbitrary
    // object and can contain anything you may need in your test.
    // This can be changed via the command line as:
    //   --params.login.user 'Joe'
    // params: {
    //     login: {
    //         user: 'Jane',
    //         password: '1234'
    //     }
    // },
    onPrepare: function() {
        browser.manage().timeouts().implicitlyWait(5000);
        browser.driver.manage().window().maximize();
        jasmine.getEnv().addReporter(reporter);
        var Reporter = require('jasmine-spec-reporter').SpecReporter;
        jasmine.getEnv().addReporter(new Reporter({
            displayFailuresSummary: true, // display summary of all failures after execution
            displayFailedSpec: true,      // display each failed spec
            displaySuiteNumber: true,    // display each suite number (hierarchical)
            displaySpecDuration: true,   // display each spec duration
            colors: {
                success: 'green',
                failure: 'red',
                pending: 'yellow'
            },
            prefixes: {
                success: '✓ ',
                failure: '✗ ',
                pending: '* '
            }
        }));
    }
};