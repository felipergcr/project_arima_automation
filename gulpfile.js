/**
 * Created by roque on 04/08/17.
 */

'use strict';

var gulp = require('gulp');
var protractor = require('gulp-protractor').protractor;

// Setting up the test task
gulp.task('protractor-run', function(callback) {
    gulp
        .src(['Tests/CT_Product/*.js'])
        .pipe(protractor({
            'configFile':__dirname + '/startTests.js',
            'debug': false,
            'autoStartStopServer': true,
            'getPageTimeout': 30000,
            'allScriptsTimeout': 30000
        }))
        .on('error', function(e) {
            console.log(e);
        })
        .on('end', callback);
});